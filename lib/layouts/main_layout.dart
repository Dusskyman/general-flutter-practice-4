import 'package:flutter/material.dart';
import 'package:general_flutter_practice_4/custom_widgets/appBar.dart';
import 'package:general_flutter_practice_4/custom_widgets/big_button.dart';
import 'package:general_flutter_practice_4/main.dart';
import 'package:general_flutter_practice_4/pages/fourth_page.dart';
import 'package:general_flutter_practice_4/pages/second_page.dart';
import 'package:general_flutter_practice_4/pages/third_page.dart';

class MainLayout extends StatelessWidget {
  final Widget body;

  const MainLayout({
    this.body,
  });
  void toPage(context, String route) {
    if (route != MyApp.currentRoute) {
      MyApp.currentRoute = route;
      Navigator.of(context).pushReplacementNamed(route);
    }
    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text('Вы уже на выбранной странице!')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Builder(
                  builder: (context) => Column(
                    children: [
                      BigButton(
                        text: 'To First Page',
                        onTap: () {
                          toPage(context, FirstPage.route);
                        },
                      ),
                      BigButton(
                        text: 'To Second Page',
                        onTap: () {
                          toPage(context, SecondPage.route);
                        },
                      ),
                      BigButton(
                        text: 'To Third Page',
                        onTap: () {
                          toPage(context, ThirdPage.route);
                        },
                      ),
                      BigButton(
                        text: 'To Fourth Page',
                        onTap: () {
                          toPage(context, FourthPage.route);
                        },
                      ),
                    ],
                  ),
                ),
              )),
        ),
      ),
      appBar: CustomAppBar(
        title: Text('This is custom AppBar'),
        tralling: Builder(
          builder: (context) => IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              }),
        ),
      ),
      body: body,
    );
  }
}
