import 'package:flutter/material.dart';

class BigButton extends StatelessWidget {
  final String text;
  final Function onTap;

  const BigButton({
    this.onTap,
    this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blueGrey[200],
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              width: 3,
              color: Colors.blueGrey[300],
            ),
          ),
          alignment: Alignment.center,
          width: double.infinity,
          height: 600,
          child: Text(
            text,
            style: TextStyle(fontSize: 25),
          ),
        ),
      ),
    );
  }
}
