import 'package:flutter/material.dart';

class CustomAppBar extends PreferredSize {
  final Widget title;
  final Widget tralling;

  @override
  Size get preferredSize => Size.fromHeight(200);

  CustomAppBar({this.title, this.tralling});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ColoredBox(
        color: Colors.blue,
        child: SizedBox(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.1,
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  title ?? Expanded(child: SizedBox()),
                  tralling ?? SizedBox(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
