import 'package:flutter/material.dart';
import 'package:general_flutter_practice_4/layouts/main_layout.dart';

class FourthPage extends StatelessWidget {
  static String route = 'FourthPage';

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      body: Center(
          child: Container(
        width: 100,
        height: 100,
        child: Text(
          'This is page #4',
          style: TextStyle(fontSize: 30),
        ),
      )),
    );
  }
}
