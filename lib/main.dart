import 'package:flutter/material.dart';
import 'package:general_flutter_practice_4/layouts/main_layout.dart';
import 'package:general_flutter_practice_4/pages/second_page.dart';
import 'package:general_flutter_practice_4/pages/third_page.dart';

import 'pages/fourth_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static String currentRoute = FirstPage.route;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        FirstPage.route: (ctx) => FirstPage(),
        SecondPage.route: (ctx) => SecondPage(),
        ThirdPage.route: (ctx) => ThirdPage(),
        FourthPage.route: (ctx) => FourthPage(),
      },
      home: FirstPage(),
    );
  }
}

class FirstPage extends StatelessWidget {
  static String route = 'FirstPage';

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      body: Center(
          child: Container(
        width: 100,
        height: 100,
        child: Text(
          'This is page #1',
          style: TextStyle(fontSize: 30),
        ),
      )),
    );
  }
}
